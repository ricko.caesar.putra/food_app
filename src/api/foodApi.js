import axios from 'axios';

export default async function queryFood(token,name) {
  try {
    const baseUrl = "https://api.irwinpratajaya.com/"
    const headers = {
        // 'Cookie': `__cfduid=da5eb80b97976d8ca37d4a2d1aa8ed7521611222135`,
        // 'Postman-Token': '<calculated when request is sent>',
        // 'Host': '<calculated when request is sent>',
        // 'User-Agent': 'PostmanRuntime/7.26.8',
        // 'Accept': '*/*',
        // 'Accept-Encoding': 'gzip, deflate, br',
        // 'Connection': 'keep-alive',
        'Authorization': `${token}`,
    }

    const result = await axios({
        method: 'GET',
        url: `${baseUrl}home?username=${name}`,
        headers
    });
    return result;
  } catch (error) {
    throw error;
  }
}

