import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from "@react-navigation/stack";
import { AddToBasket, Authentication, CompleteDetails, DrawerContent, HomeScreen, InputCardDetails, OrderComplete, OrderList, SplashScreen, TrackOrder, WelcomeScreen } from '../pages';
import { createDrawerNavigator } from '@react-navigation/drawer';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const HomeStack = () => {
    return (
        <Stack.Navigator initialRouteName="SplashScreen">
            <Stack.Screen
                name="SplashScreen"
                component={SplashScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="AddToBasket"
                component={AddToBasket}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Authentication"
                component={Authentication}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="CompleteDetails"
                component={CompleteDetails}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="HomeScreen"
                component={DrawerStack}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="InputCardDetails"
                component={InputCardDetails}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="OrderComplete"
                component={OrderComplete}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen name="OrderList"
                component={OrderList}
                options={{
                    title: "BotomTab",
                    headerShown: false
                }} />
            <Stack.Screen
                name="TrackOrder"
                component={TrackOrder}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="WelcomeScreen"
                component={WelcomeScreen}
                options={{
                    headerShown: false,
                }}
            />
            


        </Stack.Navigator>
    )
}


const DrawerStack = () => {
    return (
        <Drawer.Navigator drawerContent={props => <DrawerContent {...props}/>}>
            <Drawer.Screen name="home" component={HomeScreen}/>

        </Drawer.Navigator>
    )
}



export default class Router extends Component {
    render() {
        return (
            <NavigationContainer>
                <HomeStack />
            </NavigationContainer>
        )
    }
}