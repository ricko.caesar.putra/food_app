import queryUser from '../../api/userApi';

export default function fetchUser(name) {
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_USER_REQUEST'
    });

    try {
      const result = await queryUser(name);

      if (result.status === 200) {
        dispatch({
          type: 'FETCH_USER_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'FETCH_USER_FAILED',
          error: result.data
        });
      }
    } catch (err) {
        dispatch({
        type: 'FETCH_USER_FAILED',
        error: err
      });
    }
  };
}

