import { combineReducers } from 'redux';

import userStoreReducer from './userStoreReducer';
import foodStoreReducer from './foodStoreReducer';


const reducers = {
  userStore: userStoreReducer,
  foodStore: foodStoreReducer,
};

const rootReducer = combineReducers(reducers);

export default rootReducer;
