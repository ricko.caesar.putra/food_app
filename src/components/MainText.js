import React from 'react'
import { Text, StyleSheet } from 'react-native'

function MainText(props){
    const { text, ...others} = props

    return(
        <Text style={[
            styles.mainText,
            props.customStyle
            ]}
            {...others}>
            {text}
        </Text>
    )
}

export default MainText

const styles = StyleSheet.create({
    mainText : {
        fontWeight : '500', 
        textAlign : 'center',
        fontSize : 20,
        letterSpacing : -0.01,
        lineHeight : 29,
        marginTop : 48,
        color : '#27214D'
    },
})