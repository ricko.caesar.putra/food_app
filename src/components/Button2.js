import React from 'react'
import { TouchableOpacity, Text, View, StyleSheet } from 'react-native'
import Icons from 'react-native-vector-icons/Ionicons';

function Button(props){
    const { buttonText, icon, invert, disable, padd, ...others} = props

    return(
        <TouchableOpacity style={[
            styles.button,
            invert && styles.invert,
            props.customStyle
            ]}
            {...others}>
            <View style={{
                flexDirection:'row',
                justifyContent : 'center',
                alignItems: 'center'}}>    
                <Icons style={{paddingRight : padd}} name={icon} size={20} color='#FFFFFF'/>       
                <Text style={styles.textButton}{...others}>{buttonText}</Text>
            </View>    
        </TouchableOpacity>
    )
}

export default Button

const styles = StyleSheet.create({
    button : {
        backgroundColor : '#FFA451',
        borderRadius : 10,
        marginTop : 58,
        height : 56,
        width : '100%',
        justifyContent : 'center',
        alignSelf : 'center'
    },
    invert : {
        backgroundColor : 'red'
    },
    textButton : {
        fontSize : 16,
        textAlign: 'center',
        color : '#FFFFFF',
        lineHeight : 24,
        fontWeight : '500',
        letterSpacing : -0.01
    },
})