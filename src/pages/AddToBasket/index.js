import React, { useState } from 'react'
import { Dimensions, Image, StyleSheet, Text, View } from 'react-native'
import { colors } from '../../utils'
import { Food } from '../../assets';
import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/Ionicons'
import Button from '../../components/atoms/Button';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useEffect } from 'react';
import { connect, useSelector } from 'react-redux'

const { width, height } = Dimensions.get('window');

const AddToBasket = (props) => {
    const data = props.foodStore.payload.data.foods.filter(function (item) {
        return item.id == props.route.params.key;
    })
    const jumlah =props.foodStore.cart.filter(menu => menu.id === props.route.params.key).map(el=>el.jumlah).reduce((a,b)=>a+b,0)

    const [filterData, setFilterData] = useState(...data)
    const [jumlahPesanan, setJumlahPesanan] = useState(jumlah)
    const [totalHarga, setTotalHarga] = useState( jumlah * filterData.price)

    useEffect(() => {
        if (props.foodStore.payload.data) {
            setTotalHarga(jumlah * filterData.price)
            setJumlahPesanan(jumlah)
            setFilterData(...data)
        }
    }, [props.foodStore.payload, props.foodStore.cart])

    return (
        <View style={styles.wrapperAtas}>
            <View style={{
                left: 20,
                top: 20,
                padding: 5,
                backgroundColor: 'white',
                width: 100,
                height: 35,
                position: 'absolute',
                borderRadius: 17
            }}>
                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => {
                    props.navigation.navigate('HomeScreen')
                }}>
                    <Icon name='ios-arrow-back' size={20} color={colors.primary} />
                    <Text style={styles.GoBack}>go back</Text>
                </TouchableOpacity>
            </View>

            <Animatable.View
                duration={2000}
                animation="zoomInDown"
                easing="ease-in-sine"
                // iterationCount="infinite"
                direction='alternate'
                style={{ width: width, height: 250 }}
            >
                <Image
                    style={styles.image}
                    source={{ uri: `${filterData.img}` }}
                    height={230}
                    width={230}
                />
            </Animatable.View>

            <Animatable.View
                duration={1000}
                animation="fadeInUp"
                easing="ease-in-sine"
                // iterationCount="infinite"
                direction='alternate'
                style={styles.wrapperBawah}
            >
                <Text style={styles.text1}>{filterData.name}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => jumlahPesanan>0?props.dispatchDecrement(filterData.id,filterData.price):alert('sudah 0')}>
                            <Icon name='ios-remove-circle-outline' size={50} color={colors.primary} />
                        </TouchableOpacity>
                        <Text style={{ fontSize: 25, fontWeight: 'bold', width: 40, textAlign: 'center' }}>{jumlahPesanan}</Text>
                        <TouchableOpacity onPress={() => props.dispatchIncrement(filterData.id,filterData.price)}>
                            <Icon name='ios-add-circle' size={50} color={colors.primary} />
                        </TouchableOpacity>
                    </View>
                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Rp {totalHarga}</Text>
                </View>

                <Text style={styles.text1}>One Pack Contain:</Text>
                <Text style={styles.text2}>{filterData.ingredients}</Text>
                <Text style={styles.text2}>{filterData.description}</Text>
                <View style={{ height: 20 }} />
                <View style={{ height: 700, flexDirection: 'row', justifyContent: 'space-between' }} >
                    <TouchableOpacity onPress={()=>props.dispatchRecommended(filterData.id,filterData.recommended)}>
                        {filterData.recommended ? <Icon name='ios-heart' size={50} color={colors.primary} /> :
                            <Icon name='ios-heart' size={50} color={'grey'} />}
                    </TouchableOpacity>
                    <View style={{ flex: 1, paddingLeft: 70 }}>
                        <Button title="Add to Basket" onPress={() => props.navigation.navigate('OrderList')} type="default" />
                    </View>
                </View>
            </Animatable.View>
        </View>
    )
}

function mapStateToProps(state) {
    return {
        userStore: state.userStore,
        foodStore: state.foodStore,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchLogout: (name) => dispatch({ type: 'LOGOUT' }),
        dispatchRecommended: (id, rec) => dispatch({ type: 'RECOMMENDED', id, rec }),
        dispatchIncrement: (id,harga) => dispatch({ type: 'increment', id,harga }),
        dispatchDecrement: (id,harga) => dispatch({ type: 'decrement', id,harga }),
        // dispatchIncrement: (id) => dispatch({ type: 'increment',id }),
        dispatchFood: (token, name) => dispatch(fetchFood(token, name)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddToBasket)
// export default AddToBasket


const styles = StyleSheet.create({
    wrapperAtas: {
        backgroundColor: colors.primary,
        flex: 1,
        paddingTop: 50
    },
    wrapperBawah: {
        alignSelf: 'center',
        backgroundColor: colors.background,
        width: width,
        flex: 1,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        paddingHorizontal: 25
    },
    image: {
        alignSelf: 'center',
        marginBottom: 8,
        height: 200,
        width: 200,
        borderRadius: 115
    },
    imageSmall: {
        position: 'absolute',
        alignSelf: 'center',
        marginTop: 5,
        right: 40
    },
    text1: {
        marginTop: 40,
        fontSize: 20,
        fontWeight: 'bold',
    },
    text2: {
        marginTop: 20,
        fontSize: 14,
        fontWeight: '400',
    },
    GoBack: {
        textAlign: 'center',
        alignItems: 'center',


    }

})
