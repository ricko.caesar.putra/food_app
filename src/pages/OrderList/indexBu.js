import React, { useState, useEffect } from 'react'
import { FlatList, View, Text, TouchableOpacity, Image, ImageBackground, TextInput, ScrollView } from 'react-native'
import Icons from 'react-native-vector-icons/Ionicons'
import MainText from '../../components/MainText'
import Button from '../../components/Button2'
import { Divider } from 'react-native-elements'
import { connect } from 'react-redux';
import BottomSheet from 'reanimated-bottom-sheet';

function OrderList(props) {
    const { navigation } = props
    const [total, setTotal] = useState(0)
    const [listOrder, setListOrder] = useState([])

    useEffect(() => {
        if (props.basketStore.addedItems) {
            console.log('coba lagi : ', props.basketStore.addedItems)
            setListOrder(props.basketStore.addedItems)
            setTotal(props.basketStore.total)
        }
    }, [props.basketStore.addedItems])

    const renderItem = ({ item }) => {
        return (
            <View style={{ flexDirection: 'row', height: 120, backgroundColor: '#FFFFFF', paddingLeft: 24, paddingRight: 24, marginTop: 10, alignItems: 'center', borderBottomWidth: 1, borderBottomColor: '#F4F4F4' }}>
                <View style={{ backgroundColor: '#FFFAEB', height: 100, width: 100, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                    <Image
                        source={{ uri: item.img }}
                        style={{ height: 60, width: 60, borderRadius: 60 / 2 }}
                    />
                </View>
                <View style={{ marginLeft: 16, justifyContent: 'center' }}>
                    <Text style={{ fontSize: 16, fontWeight: 'bold' }}>
                        {item.name}
                    </Text>
                    <Text style={{ fontSize: 14 }}>
                        {item.quantity} packs
                    </Text>
                    <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
                        <Text>
                            Subtotal :
                        </Text>
                        <Text style={{ fontSize: 16, fontWeight: 'bold', marginLeft: 5 }}>
                            {item.price * item.quantity}
                        </Text>
                    </View>
                </View>
            </View>
        )
    }

    const renderContent = () => (
        <View
            style={{
                backgroundColor: '#FFFFFF',
                padding: 16,
                height: 400,
            }}
        >
            <Divider style={{ backgroundColor: '#F3F1F1', height: 5, width: 75, alignSelf: 'center' }} />
            <MainText text={'Delivery Address'} customStyle={{ marginTop: 40, textAlign: 'left' }} />
            <View>
                <TextInput
                    placeholder='First Name'
                    placeholderTextColor='#C2BDBD'
                    style={{
                        backgroundColor: '#F3F1F1',
                        borderRadius: 10,
                        height: 56,
                        marginTop: 16,
                        paddingLeft: 24
                    }}
                />
            </View>
            <MainText text={'Number We Can Call'} customStyle={{ marginTop: 24, textAlign: 'left' }} />
            <View>
                <TextInput
                    placeholder='628XXXXXXXXX'
                    placeholderTextColor='#C2BDBD'
                    style={{
                        backgroundColor: '#F3F1F1',
                        borderRadius: 10,
                        height: 56,
                        marginTop: 16,
                        paddingLeft: 24
                    }}
                />
            </View>
            <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'space-between' }}>
                <TouchableOpacity style={{ backgroundColor: 'white', height: 60, width: 150, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 10, borderColor: '#FFA451' }}
                    onPress={() => navigation.navigate('OrderComplete')}>
                    <Text style={{ fontSize: 16, color: '#FFA451' }}>
                        Pay on Delivery
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ backgroundColor: 'white', height: 60, width: 150, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 10, borderColor: '#FFA451' }}
                    onPress={() => payWithCard()}>
                    <Text style={{ fontSize: 16, color: '#FFA451' }}>
                        Pay with Card
                    </Text>
                </TouchableOpacity>
            </View>

        </View>
    );
    const sheetRef = React.useRef(null);

    const renderContentCard = () => (
        <View
            style={{
                backgroundColor: '#FFFFFF',
                // padding: 16,
                height: 550,
            }}
        >
            <View style={{ padding: 16 }}>



                <Divider style={{ backgroundColor: '#F3F1F1', height: 5, width: 75, alignSelf: 'center' }} />
                <MainText text={'Card Holders Name'} customStyle={{ marginTop: 40, textAlign: 'left' }} />
                <View>
                    <TextInput
                        placeholder='Adholpus Chris'
                        placeholderTextColor='#C2BDBD'
                        style={{
                            backgroundColor: '#F3F1F1',
                            borderRadius: 10,
                            height: 56,
                            marginTop: 16,
                            paddingLeft: 24
                        }}
                    />
                </View>
                <MainText text={'Card Number'} customStyle={{ marginTop: 24, textAlign: 'left' }} />
                <View>
                    <TextInput
                        placeholder='1234 5678 9012 1314'
                        placeholderTextColor='#C2BDBD'
                        style={{
                            backgroundColor: '#F3F1F1',
                            borderRadius: 10,
                            height: 56,
                            marginTop: 16,
                            paddingLeft: 24
                        }}
                    />
                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View>
                        <MainText text={'Date'} customStyle={{ marginTop: 24, textAlign: 'left' }} />
                        <View>
                            <TextInput
                                placeholder='10/30'
                                placeholderTextColor='#C2BDBD'
                                style={{
                                    backgroundColor: '#F3F1F1',
                                    borderRadius: 10,
                                    height: 56,
                                    width: 160,
                                    marginTop: 16,
                                    paddingLeft: 24
                                }}
                            />
                        </View>
                    </View>
                    <View>
                        <MainText text={'CCV'} customStyle={{ marginTop: 24, textAlign: 'left' }} />
                        <View>
                            <TextInput
                                placeholder='123'
                                placeholderTextColor='#C2BDBD'
                                style={{
                                    backgroundColor: '#F3F1F1',
                                    borderRadius: 10,
                                    height: 56,
                                    width: 160,
                                    marginTop: 16,
                                    paddingLeft: 24
                                }}
                            />
                        </View>
                    </View>
                </View>

            </View>

            <View style={{
                width: '100%', height: 90, marginTop: 30,
                borderTopLeftRadius: 24,
                borderTopRightRadius: 24,
                backgroundColor: '#FFA451',
                alignItems: 'center',
                justifyContent: 'center'
            }}>
                <TouchableOpacity style={{ backgroundColor: 'white', height: 60, width: 150, justifyContent: 'center', alignItems: 'center', borderWidth: 1, borderRadius: 10, borderColor: '#FFA451' }}
                    onPress={() => navigation.navigate('OrderComplete')}>
                    <Text style={{ fontSize: 16, color: '#FFA451' }}>
                        Complete Order
                    </Text>
                </TouchableOpacity>
            </View>

        </View>
    );
    const sheetRefCard = React.useRef(null);

    const payWithCard = () => {
        sheetRef.current.snapTo(2)
        sheetRefCard.current.snapTo(0)
    }

    console.log('ini order nya', listOrder)
    return (
        <>
            <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                <View style={{ backgroundColor: '#FFA451', height: 120, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity
                        style={{
                            backgroundColor: '#FFFFFF',
                            width: 100, height: 40,
                            justifyContent: 'center',
                            borderRadius: 100,
                            marginLeft: 24
                        }}
                        onPress={() => navigation.navigate('HomeScreen')}>
                        {/* onPress={() => alert('berhasil')}> */}
                        <View style={{ flexDirection: 'row' }}>
                            <Icons name='chevron-back-outline' size={25} color={'#000000'} />
                            <Text style={{ fontSize: 16, fontWeight: '500' }}>
                                Go Back
                        </Text>
                        </View>
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 24, color: '#FFFFFF', fontWeight: '500', textAlign: 'center' }}>
                            My Basket
                    </Text>
                    </View>
                    <View
                        style={{
                            backgroundColor: '#FFA451',
                            width: 100, height: 40,
                            justifyContent: 'center',
                            borderRadius: 100,
                            marginRight: 24
                        }}>
                    </View>
                </View>

                <View style={{ flex: 1, borderTopLeftRadius: 30, borderTopRightRadius: 30, backgroundColor: '#FFFFFF', marginBottom: 10 }}>
                    <FlatList
                        data={listOrder}
                        renderItem={renderItem}
                        keyExtractor={list => list.id.toString()}
                    />
                </View>


                <View style={{ paddingLeft: 25, paddingRight: 25, marginBottom: 16, alignItems: 'center', height: 75, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%', backgroundColor: '#FFFFFF', alignItems: 'center' }}>
                        <View>
                            <Text>
                                Total
                            </Text>
                            <Text style={{ fontSize: 24, fontWeight: 'bold' }}>
                                {total}
                            </Text>
                        </View>
                        <Button buttonText={`Checkout`}
                            customStyle={{ marginTop: 0, marginLeft: 24, width: '50%' }}
                            onPress={() => sheetRef.current.snapTo(0)}
                        />
                    </View>
                </View>
            </View>
            <BottomSheet
                ref={sheetRef}
                initialSnap={2}
                snapPoints={[400, 200, 0]}
                borderRadius={20}
                renderContent={renderContent}
                onCloseEnd={payWithCard}

            />
            <BottomSheet
                ref={sheetRefCard}
                initialSnap={2}
                snapPoints={[550, 250, 0]}
                borderRadius={20}
                renderContent={renderContentCard}
            />
        </>
    )
}

function mapStateToProps(state) {
    return {
        basketStore: state.basketStore
    }
}

export default connect(mapStateToProps, null)(OrderList)