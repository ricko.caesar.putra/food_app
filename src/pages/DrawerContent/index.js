import AsyncStorage from '@react-native-async-storage/async-storage'
import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { connect } from 'react-redux'
import { Button } from '../../components'

const DrawerContent = (props) => {

    
const handleLogOut = async ()=>{
    try {
        props.dispatchLogout();
        // console.warn('dispatch redux logout success')
    } catch (e) {
        console.warn(`dispatch redux logout failed`)
    }

    try {
        await AsyncStorage.clear()
        // console.warn('asynch Storage successfully cleared!')
    } catch (e) {
        console.warn('Failed to clear the async storage.')
    }
    props.navigation.reset({
        index: 0,
        routes: [{ name: 'WelcomeScreen' }],
    })
}    

    return (
        <View style={{height:'100%',justifyContent:'center',alignContent:'center'}}>
            <Button title={'LOGOUT'} onPress={()=> handleLogOut()} type={'default'}/>
        </View>
    )
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchLogout: (name) => dispatch({ type: 'LOGOUT' }),        
    }
}


export default connect(null, mapDispatchToProps)(DrawerContent)

const styles = StyleSheet.create({})
