import React, { useState } from 'react'
import { StyleSheet, Text, View,TextInput } from 'react-native'
import {TouchableOpacity } from 'react-native-gesture-handler'
import { colors } from '../../../utils'
import Icon from 'react-native-vector-icons/Ionicons'


const Input = ({ type, placeholder, notShowPassword, onPress, ...rest }) => {

    const [focusEmail,setFocusEmail]=useState(false)

    if (type == 'email'||type == 'name'||type == 'age'||type=='search') {
        return (
            <View style={styles.wrapper(focusEmail)}>
                {type=='email'? <Icon name='ios-mail' size={20} color={'#2E3E5C'} />:null}
                {type=='name'?<Icon name='ios-person' size={20} color={'#2E3E5C'} />:null}
                {type=='age'?<Icon name='ios-hand-left' size={20} color={'#2E3E5C'} />:null}
                {type=='search'?<Icon name='ios-search' size={20} color={'#2E3E5C'} />:null}
                <TextInput
                    style={styles.textInput}
                    placeholder={placeholder}
                    placeholderTextColor={colors.outline}
                    onFocus={()=>setFocusEmail(true)}
                    onBlur={()=>setFocusEmail(false)}
                    {...rest}
                />
            </View>
        )
    }
}

export default Input


const styles = {
    wrapper: (focus)=>{ 
        return{
        // flex:1,
        flexDirection: 'row',
        height: 56,
        borderWidth: 2,
        borderColor: focus? colors.primary:colors.inputText,
        borderRadius: 10,
        paddingLeft: 26,
        alignItems: 'center',
        backgroundColor:colors.inputText
    }},
    wrapperVer:(focus)=>{
        return{
        width: 72,
        height: 72,
        borderWidth: 2,
        borderColor: focus?'#1FCC79':colors.outline,
        borderRadius: 20,
        alignItems: 'center'
        }
    },
    textInput: {
        flex: 1,
        fontSize: 15,
        marginLeft: 13
    },
    textInputVar:{
        fontSize: 32,
        alignContent:'center',
        justifyContent:'center'
    }
}
