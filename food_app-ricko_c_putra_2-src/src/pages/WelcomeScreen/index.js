import React, { useEffect } from 'react'
import { Dimensions, Image, StyleSheet, Text, View, BackHandler, Alert } from 'react-native'
import { colors } from '../../utils'
import { WelcomeScreenImage, WelcomeScreenImageShadow, WelcomeScreenImageSmall } from '../../assets';
import * as Animatable from 'react-native-animatable';
import Button from '../../components/atoms/Button';
import { useFocusEffect } from '@react-navigation/native';

const { width, height } = Dimensions.get('window');

const WelcomeScreen = (props) => {


    return (
        <View style={styles.wrapperAtas}>
            <Animatable.View
                duration={2000}
                animation="zoomInDown"
                // animation="pulse"
                easing="ease-in-sine"
                // easing="ease-out" 
                // iterationCount="infinite"
                direction='alternate'
                style={{ width: width, height: 335 }}
            >
                <Image
                    style={styles.imageSmall}
                    source={WelcomeScreenImageSmall}
                    height={37.5}
                    width={50}
                />
                <Image
                    style={styles.image}
                    source={WelcomeScreenImage}
                    height={260}
                    width={301}
                />
                <Image
                    style={styles.image}
                    source={WelcomeScreenImageShadow}
                    height={12}
                    width={301}
                />
            </Animatable.View>

            <Animatable.View
                duration={1000}
                animation="fadeInUp"
                // animation="pulse"
                easing="ease-in-sine"
                // easing="ease-out" 
                // iterationCount="infinite"
                direction='alternate'
                style={styles.wrapperBawah}
            >
                <Text style={styles.text1}>Get The Freshest Fruit Salad Combo</Text>
                <Text style={styles.text2}>We deliver the best and freshest fruit salad in town. Order for a combo today!!!</Text>
                <View style={{ height: 40 }} />
                <Button
                    title="Let's Continue"
                    type="default"
                    onPress={() => {
                        props.navigation.navigate('Authentication')
                        // BackHandler.removeEventListener('hardwareBackPress', backAction)
                    }}
                />
            </Animatable.View>

        </View>
    )
}

export default WelcomeScreen

const styles = StyleSheet.create({
    wrapperAtas: {
        backgroundColor: colors.primary,
        flex: 1,
        paddingTop: 134
    },
    wrapperBawah: {
        alignSelf: 'center',
        backgroundColor: colors.background,
        width: width,
        flex: 1,
        borderTopStartRadius: 20,
        borderTopEndRadius: 20,
        paddingHorizontal: 25
    },
    image: {
        alignSelf: 'center',
        marginBottom: 8,
    },
    imageSmall: {
        position: 'absolute',
        alignSelf: 'center',
        marginTop: 5,
        right: 40
    },
    text1: {
        marginTop: 40,
        fontSize: 18,
        fontWeight: 'bold',
    },
    text2: {
        marginTop: 20,
        fontSize: 14,
        fontWeight: '400',
    }

})
