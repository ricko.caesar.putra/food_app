import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, Touchable, View } from 'react-native'
import { FlatList, ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/Ionicons'
import Pressable from 'react-native/Libraries/Components/Pressable/Pressable'
import { Input, Option } from '../../components'
import RecomendedCard from '../../components/RecomendedCard'
import { colors } from '../../utils'
import * as Animatable from 'react-native-animatable';
import { connect, useSelector } from 'react-redux'
import fetchFood from '../../stores/actions/foodAction';
import AsyncStorage from '@react-native-async-storage/async-storage';
import fetchUser from '../../stores/actions/userAction'
import { addToCart, subtractQuantity } from '../../stores/actions/basketActions'


const HomeScreen = (props) => {
    const [foodOption, setFoodOption] = useState('hottest')
    const [dataFood, setDataFood] = useState([])
    const [jumlahPesanan, setJumlahPesanan] = useState(0)
    const [username, setUsername] = useState('');
    const [isSearch, setIsSearch] = useState(false)
    const [dataFoodSearch, setDataFoodSearch] = useState([])
    const [foodSearch, setFoodSearch] = useState('')

    useEffect(() => {
        getData()
    }, [])

    useEffect(() => {
        if (props.foodStore.payload.data) {
            setDataFood(props.foodStore.payload.data.foods)
            setJumlahPesanan(props.foodStore.cart.map(el => el.jumlah).reduce((a, b) => a + b, 0))
        }
    }, [props.foodStore.payload, props.foodStore.cart])

    const getData = async () => {
        try {
            const token = await AsyncStorage.getItem('@token')
            const name = await AsyncStorage.getItem('@name')
            setUsername(name)
            props.dispatchFood(token, name)
            props.dispatchUser(name)
        } catch (e) {
        }
    }

    return (
        <View style={styles.wrapper}>
            <View style={styles.barAtas}>
                <TouchableOpacity onPress={() => props.navigation.navigate('HomeScreen')}>
                    <Icon name='ios-menu' size={40} color={'#2E3E5C'} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => props.navigation.navigate('OrderList')}>
                    <View style={styles.bucket}>
                        <Icon name='ios-logo-bitbucket' size={40} color={colors.primary} />
                        <Text style={styles.notifBucket}>{jumlahPesanan}</Text>
                        <Text style={styles.textBucket}>My Bucket</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <Text style={styles.GreetingText}>Hello {username}, What fruit salad combo do you want today</Text>


            <View style={{ flexDirection: 'row',paddingHorizontal:20 }}>

                <View style={{ flex: 1, marginRight: 20 }}><Input
                    type="search"
                    placeholder="search for fruit salad combo"
                    value={foodSearch}
                    onChangeText={(value) => {
                        setFoodSearch(value)
                        const dataSearch = dataFood.filter((item) => item.name.toLowerCase().includes(value.toLowerCase()))
                        setDataFoodSearch(dataSearch)
                        setIsSearch(true)
                    }}
                /></View>

                <TouchableOpacity onPress={() => props.dispatchLogout()}>
                    <Icon name='ios-filter' size={40} color={colors.primary} />
                </TouchableOpacity>
            </View>
            <View style={{ height: 10 }} />

            {isSearch ?
                <View>
                    <ScrollView horizontal>
                        {dataFoodSearch.map(item => {
                            return <RecomendedCard
                                key={item.id}
                                price={item.price}
                                name={item.name}
                                img={item.img}
                                recommended={item.recommended}
                                recommendedPress={() => props.dispatchRecommended(item.id, item.recommended)}
                                addPress={() => props.dispatchIncrement(item.id, item.price)}
                                detailPress={(key) => props.navigation.navigate('AddToBasket', { key: item.id })}
                            />
                        })}
                    </ScrollView>
                    <TouchableOpacity
                                onPress={() => {
                                    setIsSearch(false)
                                }}>
                                <View style={styles.backButton}>
                                    <Text style={styles.backText}>Back</Text>
                                </View>
                            </TouchableOpacity>
                </View>
                :

                <View style={{paddingLeft:10}}>
                    <Text style={styles.GreetingText}>Recommended Combo</Text>

                    <ScrollView horizontal>
                        {dataFood.filter((item) => item.recommended == true).map(item => {
                            return <RecomendedCard
                                key={item.id}
                                price={item.price}
                                name={item.name}
                                img={item.img}
                                recommended={item.recommended}
                                recommendedPress={() => props.dispatchRecommended(item.id, item.recommended)}
                                addPress={() => props.dispatchIncrement(item.id, item.price)}
                                detailPress={(key) => props.navigation.navigate('AddToBasket', { key: item.id })}
                            />
                        })}
                    </ScrollView>

                    <View style={{ height: 30 }} />

                    <Option
                        options={['hottest', 'popular', 'newCombo', 'top']}
                        onChange={(option, index) => {
                            setFoodOption(option);
                        }}
                    />

                    <ScrollView horizontal>
                        {dataFood.filter((item) => item[foodOption] == true).map(item => {
                            return <RecomendedCard
                                key={item.id}
                                price={item.price}
                                name={item.name}
                                img={item.img}
                                recommended={item.recommended}
                                recommendedPress={() => props.dispatchRecommended(item.id, item.recommended)}
                                addPress={() => props.dispatchIncrement(item.id, item.price)}
                                detailPress={(key) => props.navigation.navigate('AddToBasket', { key: item.id })}
                            />
                        })}
                    </ScrollView>
                </View>
            }

        </View>
    )
}

function mapStateToProps(state) {
    return {
        userStore: state.userStore,
        foodStore: state.foodStore,
        basketStore: state.basketStore,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchLogout: (name) => dispatch({ type: 'LOGOUT' }),
        dispatchRecommended: (id, rec) => dispatch({ type: 'RECOMMENDED', id, rec }),
        dispatchIncrement: (id, harga) => dispatch({ type: 'increment', id, harga }),
        dispatchFood: (token, name) => dispatch(fetchFood(token, name)),
        dispatchUser: (name) => dispatch(fetchUser(name)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)

const styles = StyleSheet.create({
    wrapper: {
        // paddingLeft: 20, 
        backgroundColor: 'white',
        // flex: 1
    },

    barAtas: {
        marginTop: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textBucket: {
        fontSize: 10,
    },
    bucket: {
        alignItems: 'center'
    },
    notifBucket: {
        position: 'absolute',
        right: 5,
        width: 25,
        height: 25,
        borderRadius: 15,
        backgroundColor: 'red',
        color: 'black',
        textAlign: 'center',
        justifyContent: 'center',
        fontWeight: 'bold'
    },
    GreetingText: {
        width: 300,
        marginTop: 30,
        fontSize: 17,
        fontWeight: 'bold',
        paddingHorizontal: 20,
        marginBottom: 10

    },
    wrapperCustom: {
        borderRadius: 8,
        padding: 6,
        width: 100,
        alignItems: 'center',
        marginRight: 10,
        height: 35
    },
    backText: {
        color: 'white',
        textAlign: 'center',
        fontWeight:'bold',
        fontSize:15
    },
    backButton: {
        marginTop:20,
        backgroundColor:'black',
        alignItems: 'center',
        alignSelf:'center',
        justifyContent: 'center',
        height:40,
        width:200,
        borderRadius:20
    }
})
