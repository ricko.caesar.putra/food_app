import { ActivityIndicator } from "react-native";

const defaultState = {
  payload: {},
  isLoading: false,
  cart: [],
  error: {}
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case 'FETCH_FOOD_REQUEST': {
      return {
        ...state,
        isLoading: true
      };
    }

    case 'FETCH_FOOD_SUCCESS': {
      return {
        ...state,
        payload: action.payload,
        isLoading: false
      };
    }

    case 'FETCH_FOOD_FAILED': {
      return {
        ...state,
        payload: {},
        error: action.error,
        isLoading: false
      };
    }

    case 'LOGOUT': {
      return { ...defaultState };
    }
    default:
      return state;

    case 'RECOMMENDED': {
      return {
        ...state,
        payload: {
          ...state.payload,
          data: {
            ...state.payload.data,
            foods:
              state.payload.data.foods.map(el => (
                el.id == action.id ? {
                  ...el,
                  recommended: !action.rec
                } : el
              ))
          }
        }
      };
    }

    case 'increment': {
      return {
        ...state,
        cart:[
          ...state.cart,
          {id:action.id, jumlah:1, harga:action.harga}
        ]
      };
    }


    case 'decrement': {
      return {
        ...state,

        cart:[
          ...state.cart,
          {id:action.id, jumlah:-1, harga:-action.harga}
        ]
      };
    }

  }
};

