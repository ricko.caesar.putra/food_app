import { combineReducers } from 'redux';

import userStoreReducer from './userStoreReducer';
import foodStoreReducer from './foodStoreReducer';
import basketReducer from './basketReducers'

const reducers = {
  userStore: userStoreReducer,
  foodStore: foodStoreReducer,
  basketStore : basketReducer
};

const rootReducer = combineReducers(reducers);

export default rootReducer;
