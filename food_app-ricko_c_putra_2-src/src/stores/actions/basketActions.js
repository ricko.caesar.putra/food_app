export function addToCart(id){
    
    return async (dispatch) => {
        dispatch({
            type : 'ADD_TO_CART',
            id : id,
        })
    }
}

export function subtractQuantity(id){
    return async (dispatch) => {
        dispatch({
            type : 'SUB_QUANTITY',
            id: id
        })
    }
}