import queryFood from '../../api/foodApi';

export default function fetchFood(token,name) {
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_FOOD_REQUEST'
    });

    try {
      const result = await queryFood(token,name);

      if (result.status === 200) {
        dispatch({
          type: 'FETCH_FOOD_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'FETCH_FOOD_FAILED',
          error: result.data
        });
      }
    } catch (err) {
        dispatch({
        type: 'FETCH_FOOD_FAILED',
        error: err
      });
    }
  };
}
